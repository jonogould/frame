<?php namespace JonoGould\Frame;

use Closure;
use Illuminate\Container\Container;
use Illuminate\Http\Request;

class Application extends Container
{
  protected $basePath;
  protected $config;
  protected $router;

  public function __construct(array $context = []) {
    $this
      ->initContainer()
      ->registerConfig($context)
      ->registerDispatcher()
      ->registerRouter();
  }

  public function __invoke() {
    return $this->run();
  }

  /**
   * Initialise the application container.
   *
   * @return void
   */
  protected function initContainer()
  {
    static::setInstance($this);
    $this->instance('app', $this);

    return $this;
  }

  protected function registerConfig(array $config)
  {
    $this->bindShared('config', function() use ($config) {
      return (new Config($config))->setApplication($this);
    });

    $this->config = $config = $this['config'];

    //  Set the base path
    $this->basePath = $config('paths.base');

    return $this;
  }

  protected function registerDispatcher()
  {
    $this->bindShared('dispatcher', function($this) {
      return new \Illuminate\Events\Dispatcher;
    });

    return $this;
  }

  protected function registerRouter()
  {
    $this->bindShared('router', function($this) {
      return new \Illuminate\Routing\Router($this['dispatcher']);
    });

    return $this;
  }

  /**
   * Run the application to deal with responses
   *
   * @return void
   */
  public function run()
  {
    $this->router = $this['router'];

    $request = Request::createFromGlobals();
    echo '<pre>';
      var_dump($request->getMethod());
    echo '</pre>';
    $response = $this->router->dispatch($request);
    $response->send();

    return true;
  }

  public function welcome() {
    return 'Hello World!';
  }
}