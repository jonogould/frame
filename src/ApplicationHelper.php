<?php namespace JonoGould\Frame;

trait ApplicationHelper
{
  protected $app;

  public function setApplication(Application $app)
  {
    $this->app = $app;

    return $this;
  }
}