<?php namespace JonoGould\Frame;

class Config
{
  use ApplicationHelper;

  protected $config = [];

  public function __construct(array $config = [])
  {
    $this->config = $config;

    return $this;
  }

  public function __invoke($key, $default = null)
  {
    return $this->get($key, $default);
  }

  public function extend(array $config)
  {
    $this->config = array_merge($this->config, $config);

    return $this;
  }

  public function get($key, $default = null)
  {
    return static::fromDotNotation($this->config, $key, $default);
  }

  public function set($key, $value)
  {
    $this->config[$key] = $value;
    return $this;
  }

  public function setAll($values)
  {
    $this->config = array_merge($this->config, $values);
  }

  public static function fromDotNotation($array, $key, $default = null)
  {
    if (isset($array[$key]))
      return $array[$key];

    foreach (explode(".", $key) as $part) {
      if (!is_array($array) || !array_key_exists($part, $array)) {
        return $default;
      }

      $array = $array[$part];
    }

    return $array;
  }
}